extends KinematicBody2D


export var speed = 200
export var dashMultiplier = 5
export var knockbackSpeed = 500
export var dashCooldown = 1
export var attackCooldown = 0.5
export var maxHitpoints = 9
export var hitpoints = 9

enum ANIMATION { IDLE_RIGHT = 0, IDLE_LEFT = 1, WALK_RIGHT = 2, WALK_LEFT = 3, DAMAGED_LEFT = 4, DAMAGED_RIGHT = 5, ATTACK_LEFT = 6, ATTACK_RIGHT = 7 }
enum DIRECTION { RIGHT = 0, SE = 1, DOWN = 2, SW = 3, LEFT = 4, NW = 5, UP = 6, NE = 7 }
enum ITEM { SWORD = 0, GUN = 1 }

# child node references
var sprite
var playerHitbox
var meleeArea
var meleeHitbox
var meleeAnimation
var bulletSpawn
var gunSprite
var gunAnimation
var pickupArea

# members
var currentDirection = DIRECTION.DOWN
var lastDirection = DIRECTION.DOWN
var lastSide = DIRECTION.RIGHT
var inventory = [ITEM.SWORD, ITEM.GUN]
var currentSlot = 0
var currentItem = inventory[currentSlot]
var knockbackVec

# player states
var isMoving = false
var isDashing = false
var isDashInvuln = false
var canDash = true
var isShooting = false
var isAttacking = false
var canAttack = true
var isInvuln = false
var invulnBlink = false
var isDamaged = false
var isInKnockback = false
var isDead = false

# timers
var dashCooldownTimer
var dashLengthTimer
var attackCooldownTimer
var attackTimer
var damageTimer
var invulnTimer
var blinkTimer
var knockbackTimer
var gunAnimTimer

# signals
signal damageTaken
signal healed
signal lifeGained
signal slotChanged
signal shotFired
signal enemyHit
signal plantHarvested
signal playerDeath
signal pickup

func _ready():
	sprite = $PlayerSprite
	playerHitbox = $PlayerCollider
	meleeArea = $MeleeArea
	meleeHitbox = $MeleeArea/MeleeCollider
	meleeAnimation = $MeleeArea/SwingAnimation
	bulletSpawn = $ShotSpawn
	gunSprite = $GunSprite
	gunAnimation = $GunSprite/GunAnimation
	pickupArea = $PickupCollider
	
	gunSprite.visible = false
	
	# instantiate timers for various player actions
	# dash timers
	dashCooldownTimer = Timer.new()
	dashCooldownTimer.set_wait_time(dashCooldown)
	dashCooldownTimer.one_shot = true
	dashCooldownTimer.connect('timeout', self, 'onDashCooledDown')
	
	dashLengthTimer = Timer.new()
	dashLengthTimer.set_wait_time(0.1)
	dashLengthTimer.one_shot = true
	dashLengthTimer.connect('timeout', self, 'onDashOver')
	
	# slash timers
	attackCooldownTimer = Timer.new()
	attackCooldownTimer.set_wait_time(attackCooldown)
	attackCooldownTimer.one_shot = true
	attackCooldownTimer.connect('timeout', self, 'onAttackCooledDown')
	
	attackTimer = Timer.new()
	attackTimer.set_wait_time(0.25)
	attackTimer.one_shot = true
	attackTimer.connect('timeout', self, 'onAttackOver')
	
	# damage invulnerability timer
	invulnTimer = Timer.new()
	invulnTimer.set_wait_time(2)
	invulnTimer.one_shot = true
	invulnTimer.connect('timeout', self, 'onInvulnOver')
	
	blinkTimer = Timer.new()
	blinkTimer.set_wait_time(0.075)
	blinkTimer.connect('timeout', self, 'blinkToggle')
	
	# damage animation timer
	damageTimer = Timer.new()
	damageTimer.set_wait_time(0.15)
	damageTimer.one_shot = true
	damageTimer.connect('timeout', self, 'onDamageOver')
	
	knockbackTimer = Timer.new()
	knockbackTimer.set_wait_time(0.15)
	knockbackTimer.one_shot = true
	knockbackTimer.connect('timeout', self, 'onKnockbackOver')
	
	# gun animation timer
	gunAnimTimer = Timer.new()
	gunAnimTimer.set_wait_time(0.25)
	gunAnimTimer.one_shot = true
	gunAnimTimer.connect('timeout', self, 'onGunAnimOver')
	
	# add timers to the player
	add_child(dashCooldownTimer)
	add_child(dashLengthTimer)
	add_child(attackCooldownTimer)
	add_child(attackTimer)
	add_child(invulnTimer)
	add_child(blinkTimer)
	add_child(damageTimer)
	add_child(knockbackTimer)
	add_child(gunAnimTimer)
	
	# connect collision signals
	meleeArea.connect('body_entered', self, 'checkMeleeCollision')
# end _ready
	
func _process(delta):
	if not isDead:
		# invulnerability blinking logic
		if isInvuln:
			if invulnBlink:
				self.visible = false
			else:
				self.visible = true
		
		if isInKnockback:
			move_and_slide(knockbackVec * knockbackSpeed)
		else:
			determineInputs()
		
		var collision = move(delta)
	else:
		self.visible = false
# end _process

func _input(event):
	if event.is_action_pressed('next_inventory'):
		# switch to the next inventory slot
		changeCurrentItem(currentSlot + 1)
	
	if event.is_action_pressed('prev_inventory'):
		# switch to the previous inventory slot
		changeCurrentItem(currentSlot - 1)
# end _input

# detects collisions from attacks, not player on enemy collisions
func checkMeleeCollision(collider):
	if not collider == null and not meleeHitbox.disabled:
		if collider.is_in_group('enemies'):
			emit_signal('enemyHit', collider, self.global_position)
		if collider.is_in_group('plants'):
			print('harvesting plant')
			emit_signal('plantHarvested', collider)
# end checkCollisions

func onPlayerHit(enemyPos):
	if not isInvuln and not isDashInvuln:
		isDamaged = true
		invulnTimer.start()
		blinkTimer.start()
		damageTimer.start()
		if hitpoints > 0:
			hitpoints -= 1

		if hitpoints == 0:
			emit_signal('playerDeath')
			isDead = true
		emit_signal('damageTaken', hitpoints)
		isInvuln = true
		knockback(enemyPos)
# end onPlayerHti

# pushes the player in the opposite direction of the supplied vector
func knockback(vec):
	self.modulate = Color('#f64747')
	var damageAnim = ANIMATION.DAMAGED_RIGHT if lastSide == DIRECTION.RIGHT else ANIMATION.DAMAGED_LEFT
	knockbackVec = (self.global_position - vec).normalized()
	
	isDamaged = true
	isInKnockback = true
	knockbackTimer.start()
	damageTimer.start()
# end knockback

func determineInputs():
	# movement related inputs
	
	if not isInKnockback:
		# primary directions
		if Input.is_action_pressed('ui_left'):
			# move left
			currentDirection = DIRECTION.LEFT
			lastDirection = currentDirection
			lastSide = DIRECTION.LEFT
			isMoving = true
			animate(ANIMATION.WALK_LEFT)
		
		elif Input.is_action_pressed('ui_right'):
			# move right
			currentDirection = DIRECTION.RIGHT
			lastDirection = currentDirection
			lastSide = DIRECTION.RIGHT
			isMoving = true
			animate(ANIMATION.WALK_RIGHT)
		
		if Input.is_action_pressed('ui_down'):
			# move down
			currentDirection = DIRECTION.DOWN
			lastDirection = currentDirection
			isMoving = true
			var walkAnim = ANIMATION.WALK_LEFT if lastSide == DIRECTION.LEFT else ANIMATION.WALK_RIGHT
			animate(walkAnim)
		
		elif Input.is_action_pressed('ui_up'):
			# move up
			currentDirection = DIRECTION.UP
			lastDirection = currentDirection
			isMoving = true
			var walkAnim = ANIMATION.WALK_LEFT if lastSide == DIRECTION.LEFT else ANIMATION.WALK_RIGHT
			animate(walkAnim)
		
		# diagonal movement
		if (Input.is_action_pressed('ui_left') and
			Input.is_action_pressed('ui_up')):
				currentDirection = DIRECTION.NW
				lastDirection = currentDirection
				lastSide = DIRECTION.LEFT
				isMoving = true
				animate(ANIMATION.WALK_LEFT)
		
		elif (Input.is_action_pressed('ui_right') and
			Input.is_action_pressed('ui_up')):
				currentDirection = DIRECTION.NE
				lastDirection = currentDirection
				lastSide = DIRECTION.RIGHT
				isMoving = true
				animate(ANIMATION.WALK_RIGHT)
		
		elif (Input.is_action_pressed('ui_left') and
			Input.is_action_pressed('ui_down')):
				currentDirection = DIRECTION.SW
				lastDirection = currentDirection
				lastSide = DIRECTION.LEFT
				isMoving = true
				animate(ANIMATION.WALK_LEFT)
		
		elif (Input.is_action_pressed('ui_right') and
			Input.is_action_pressed('ui_down')):
				currentDirection = DIRECTION.SE
				lastDirection = currentDirection
				lastSide = DIRECTION.RIGHT
				isMoving = true
				animate(ANIMATION.WALK_RIGHT)
		
		if Input.is_action_just_pressed('dash') and canDash:
			isDashing = true
			isDashInvuln = true
			canDash = false
			dashCooldownTimer.start()
			dashLengthTimer.start()
		
		# no movement buttons are being pressed
		if (not Input.is_action_pressed('ui_left') and
			not Input.is_action_pressed('ui_right') and
			not Input.is_action_pressed('ui_down') and
			not Input.is_action_pressed('ui_up')):
				isMoving = false
				var idleAnim = ANIMATION.IDLE_LEFT if lastSide == DIRECTION.LEFT else ANIMATION.IDLE_RIGHT
				animate(idleAnim)
		
		
		# combat related inputs
		if Input.is_action_just_pressed('attack'):
			# attack based on currently held inventory item
			var attackAnim = ANIMATION.ATTACK_LEFT if lastSide == DIRECTION.LEFT else ANIMATION.ATTACK_RIGHT
			attack()
	
	
	# inventory related inputs
	if Input.is_action_just_pressed('inventory_1'):
		# switch to whatever item is in slot 1
		changeCurrentItem(0)
	
	if Input.is_action_just_pressed('inventory_2'):
		# switch to whatever item is in slot 2
		changeCurrentItem(1)
	
	if Input.is_action_just_pressed('inventory_3'):
		# switch to whatever item is in slot 3
		changeCurrentItem(2)
	
	if Input.is_action_just_pressed('inventory_4'):
		# switch to whatever item is in slot 4
		changeCurrentItem(3)
# end determineInputs

func animate(anim):
	var animName
	
	match anim:
		ANIMATION.IDLE_RIGHT:
			animName = 'IdleRight'
		
		ANIMATION.IDLE_LEFT:
			animName = 'IdleLeft'
		
		ANIMATION.WALK_RIGHT:
			animName = 'WalkRight'
		
		ANIMATION.WALK_LEFT:
			animName = 'WalkLeft'
	
	if not isAttacking or not isDamaged:
		sprite.animation = animName
# end animate

func changeCurrentItem(slot):
	# handle scrolling going past the end or beginning
	if slot > inventory.size() - 1:
		slot = slot % inventory.size()
	
	if slot < 0:
		slot = inventory.size() - 1
	
	# change the current item to the specified item slot
	currentSlot = slot
	currentItem = inventory[currentSlot]
	emit_signal('slotChanged', currentSlot)
# end changeCurrentItem

func getMoveVector(direction):
	var moveVec
	
	match direction:
		DIRECTION.LEFT:
			moveVec = Vector2(-1, 0)
		
		DIRECTION.RIGHT:
			moveVec = Vector2(1, 0)
		
		DIRECTION.UP:
			moveVec = Vector2(0, -1)
		
		DIRECTION.DOWN:
			moveVec = Vector2(0, 1)
		
		DIRECTION.NW:
			moveVec = Vector2(-1, -1)
		
		DIRECTION.NE:
			moveVec = Vector2(1, -1)
		
		DIRECTION.SE:
			moveVec = Vector2(1, 1)
		
		DIRECTION.SW:
			moveVec = Vector2(-1, 1)
	
	return moveVec
# end getMoveVector

func move(delta):
	if isMoving:
		var moveVec = getMoveVector(currentDirection)
		
		moveVec = moveVec.normalized() * speed
		
		if isDashing:
			moveVec *= dashMultiplier
		
		return self.move_and_slide(moveVec)
# end move

func attack():
	# determine what attack should be done based on what the current inventory item is
	if canAttack:
		match currentItem:
			ITEM.SWORD:
				slash()
			
			ITEM.GUN:
				shoot()
# end attack

func slash():
	# slash in the direction the player is facing
	var anim
	
	match currentDirection:
		DIRECTION.RIGHT: anim = 'SwingRight'
		DIRECTION.LEFT: anim = 'SwingLeft'
		DIRECTION.UP: anim = 'SwingUp'
		DIRECTION.DOWN: anim = 'SwingDown'
		DIRECTION.NE: anim = 'SwingNE'
		DIRECTION.NW: anim = 'SwingNW'
		DIRECTION.SW: anim = 'SwingSW'
		DIRECTION.SE: anim = 'SwingSE'
	
	meleeAnimation.play(anim)
	
	attackTimer.start()
	attackCooldownTimer.start()
	isAttacking = true
	canAttack = false
	meleeHitbox.disabled = false
	meleeHitbox.show()
# end slash

func setShotSpawnPos():
	var pos
	
	match currentDirection:
		DIRECTION.LEFT:
			pos = Vector2(-1, 0)
		
		DIRECTION.RIGHT:
			pos = Vector2(1, 0)
		
		DIRECTION.UP:
			pos = Vector2(0, -1)
		
		DIRECTION.DOWN:
			pos = Vector2(0, 1)
		
		DIRECTION.NW:
			pos = Vector2(-1, -1)
		
		DIRECTION.NE:
			pos = Vector2(1, -1)
		
		DIRECTION.SE:
			pos = Vector2(1, 1)
		
		DIRECTION.SW:
			pos = Vector2(-1, 1)
	
	bulletSpawn.position = pos.normalized() * 35
# end setShotSPawnPos

func shoot():
	var anim
	
	match currentDirection:
		DIRECTION.RIGHT: anim = 'ShootRight'
		DIRECTION.LEFT: anim = 'ShootLeft'
		DIRECTION.UP: anim = 'ShootUp'
		DIRECTION.DOWN: anim = 'ShootDown'
		DIRECTION.NE: anim = 'ShootNE'
		DIRECTION.NW: anim = 'ShootNW'
		DIRECTION.SW: anim = 'ShootSW'
		DIRECTION.SE: anim = 'ShootSE'
	
	gunAnimation.play(anim)
	
	gunSprite.visible = true
	gunAnimTimer.start()
	setShotSpawnPos()
	emit_signal('shotFired', bulletSpawn.global_position, currentDirection)
	
	attackTimer.start()
	attackCooldownTimer.start()
	isAttacking = true
	canAttack = false
# end shoot

func onDashOver():
	isDashing = false
	isDashInvuln = false
# end onDashOver

func onDashCooledDown():
	canDash = true
# end onDashCooledDown

func onAttackOver():
	isAttacking = false
	meleeHitbox.disabled = true
	meleeHitbox.hide()
	meleeHitbox.rotation = 0
# end onAttackOver

func onAttackCooledDown():
	canAttack = true
# end onAttackCooledDown

func blinkToggle():
	invulnBlink = not invulnBlink
# end blinkToggle

func onInvulnOver():
	isInvuln = false
	self.visible = true
	blinkTimer.stop()
# end onInvulnOver

func onDamageOver():
	self.modulate = Color('#ffffff')
	isDamaged = false
# end onDamageOver

func onKnockbackOver():
	isInKnockback = false
# end onKnockbackOver

func onGunAnimOver():
	gunSprite.visible = false
# end onGunAnimOver

func raiseHealth(dontuse):
	hitpoints += 1
	print('healed to ' + str(hitpoints))
	emit_signal('healed', hitpoints)
# end raiseHealth