extends KinematicBody2D


export var speed = 500

enum DIRECTION { RIGHT = 0, SE = 1, DOWN = 2, SW = 3, LEFT = 4, NW = 5, UP = 6, NE = 7 }

var directionVec
var moveVec
var hasBeenFired = false
var particles

var deleteTimer

onready var sprite = $WaterSprite

signal enemyBulletHit
signal waterThrown

func _ready():
	deleteTimer = Timer.new()
	deleteTimer.set_wait_time(0.8)
	deleteTimer.one_shot = true
	deleteTimer.connect('timeout', self, 'onDelete')
	
	particles = $ThrowParticles.duplicate()
	add_child(particles)
	
	var area = $ColliderArea
	
	area.connect('body_entered', self, 'checkBulletCollisions')
	
	add_child(deleteTimer)
# end _ready

func _process(delta):
	var collision = move_and_collide(directionVec.normalized() * speed * delta)
#	checkBulletCollisions(collision)
# end _process

func directionToVector(direction):
	var vec
	
	match direction:
		DIRECTION.LEFT:
			vec = Vector2(-1, 0)
		
		DIRECTION.RIGHT:
			vec = Vector2(1, 0)
		
		DIRECTION.UP:
			vec = Vector2(0, -1)
		
		DIRECTION.DOWN:
			vec = Vector2(0, 1)
		
		DIRECTION.NW:
			vec = Vector2(-1, -1)
		
		DIRECTION.NE:
			vec = Vector2(1, -1)
		
		DIRECTION.SE:
			vec = Vector2(1, 1)
		
		DIRECTION.SW:
			vec = Vector2(-1, 1)
	
	return vec
# end directionToVector

func setDirectionVec(direction):
	directionVec = directionToVector(direction)
	
	$WaterSprite.rotate(deg2rad(direction * 45))
# end setDirectionVec

func setInitialPosition(pos):
	set_global_position(pos)
# end setInitialPosition

func checkBulletCollisions(collider):
	if not collider == null:
		if collider.is_in_group('enemies'):
			emit_signal('enemyBulletHit', collider)
		
		throwWater()
		

func throwWater():
	$WaterSprite.visible = false
	particles.emitting = true
	
	var distance = 50
	emit_signal('waterThrown', self.global_position, distance)
	
	deleteTimer.start()
# end throwWater

func onDelete():
	queue_free()
# end onDelete