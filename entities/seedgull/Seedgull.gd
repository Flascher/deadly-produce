extends KinematicBody2D


export var minSpeed = 100
export var maxSpeed = 200
export var knockbackSpeed = 500

onready var player = get_node('/root/World/Player')
onready var particleGroup = $ParticleGroup
onready var sprite = $SeedgullSprite
onready var hitbox = $SeedgullHitbox
var turnip = preload('res://entities/plants/Turnip/Turnip.tscn')

# members
var speed
var playerPos
var hitpoints = 2
var lastPlotPos
var isKnockedBack = false
var knockbackVec
var lastTarget

# states
var isStunned = false

# timers
var hurtTimer
var stunTimer
var deathTimer

# signals
signal playerHit
signal enemyDeath

func _ready():
	randomize()
	
	speed = rand_range(minSpeed, maxSpeed)
	
	stunTimer = Timer.new()
	stunTimer.set_wait_time(1)
	stunTimer.one_shot = true
	stunTimer.connect('timeout', self, 'onStunOver')
	
	hurtTimer = Timer.new()
	hurtTimer.set_wait_time(0.15)
	hurtTimer.one_shot = true
	hurtTimer.connect('timeout', self, 'onKnockbackOver')
	
	deathTimer = Timer.new()
	deathTimer.set_wait_time(1.1)
	deathTimer.one_shot = true
	deathTimer.connect('timeout', self, 'onDeathOver')
	
	add_child(stunTimer)
	add_child(hurtTimer)
	add_child(deathTimer)
# end _ready

func _process(delta):
	playerPos = player.global_position
	
	var directionVec = playerPos - self.global_position
	var moveVec = directionVec.normalized() * (speed * delta)
	
	if directionVec.x > 0:
		sprite.animation = 'WalkRight'
	else:
		sprite.animation = 'WalkLeft'
	
	var collisions
	if not isStunned:
		if not isKnockedBack:
			collisions = move_and_collide(moveVec)
		else:
			move_and_slide(knockbackVec * knockbackSpeed)
	
	if not collisions == null and collisions.collider.is_in_group('player'):
		emit_signal('playerHit', self.global_position)
# end _process

func onDamageTaken(dmgReceiver, playerPos):
	if dmgReceiver == self:
		self.modulate = Color('#f64747')
		hurtTimer.start()
		hitpoints -= 1
		
		knockbackVec = -(playerPos - self.global_position).normalized()
		knockback(knockbackVec)
		
		if hitpoints <= 0:
			sprite.visible = false
			hitbox.disabled = true
			deathTimer.start()
			
			for particle in particleGroup.get_children():
				particle.emitting = true	
				
			emit_signal('enemyDeath', lastPlotPos, self.global_position)
			
# end onDamageTaken

func onBulletHit(collider):
	if collider == self:
		stunTimer.start()
		isStunned = true
# end onBulletHit

func knockback(vec):
	isKnockedBack = true
# end knockback

func onKnockbackOver():
	isKnockedBack = false
	self.modulate = Color('#ffffff')
# end resetColor

func onStunOver():
	isStunned = false
# end onStunOver

func onDeathOver():
	queue_free()