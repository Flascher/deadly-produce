extends Node2D


# members
var growth = 1
var harvestMoveSpeed = 50
var harvestMoveVec

# states
var isHarvestMoving

# node references
onready var sprite = $TurnipSprite
onready var harvestArea = $HarvestArea
onready var harvestCollider = $HarvestArea/HarvestCollider
onready var droppedTurnip = $DroppedTurnip
onready var droppedSprite = $DroppedTurnip/DroppedSprite
onready var pickupArea = $DroppedTurnip/PickupArea
onready var pickupCollider = $DroppedTurnip/PickupArea/PickupCollider

# timers
var harvestMoveTimer

# signals
signal scoreUp

func _ready():
	randomize()
	
	var moveX = rand_range(-harvestMoveSpeed, harvestMoveSpeed)
	var moveY = rand_range(-harvestMoveSpeed, harvestMoveSpeed)
	
	harvestMoveVec = Vector2(moveX, moveY).normalized()
	
	isHarvestMoving = false
	
	harvestMoveTimer = Timer.new()
	harvestMoveTimer.set_wait_time(0.5)
	harvestMoveTimer.one_shot = true
	harvestMoveTimer.connect('timeout', self, 'harvestMoveOver')
	
	harvestArea.connect('body_entered', self, 'checkHarvestCollision')
	pickupArea.connect('body_entered', self, 'pickup')
	
	add_child(harvestMoveTimer)
# end _ready

func _process(delta):
	if isHarvestMoving:
		droppedTurnip.move_and_slide(harvestMoveVec * harvestMoveSpeed)
	else:
		droppedTurnip.move_and_slide(Vector2(0, 0))
# end _process

func setPos(plotPos, plantPos):
	if not plotPos == null:
		var plantedPos = findPlantPosition(plotPos, plantPos)
		
		if not plantedPos == null:
			self.set_position(plantedPos)
#			emit_signal('seedPlanted', self)
# end setPlotPos

func isInPlotBounds(plotPos, plantPos):
	var maxDistance = 125 # half of the plot size
	
	if ((plantPos.x < (plotPos.x + maxDistance) and
		plantPos.x > (plotPos.x - maxDistance)) and
		(plantPos.y < (plotPos.y + maxDistance) and
		plantPos.y > (plotPos.y - maxDistance))):
			return true
	else:
		return false

func findPlantPosition(plotPos, plantPos):
	# check to make sure the death position is still within the plot
	if isInPlotBounds(plotPos, plantPos):
		# align final position with tiles
		var posX = int(plantPos.x)
		var posY = int(plantPos.y)
		
		var moveLeft = int(posX) % 33 < 16
		var moveUp = int(posY) % 33 < 16
		
		var offsetX = -1 * (posX % 33) if moveLeft else posX % 33
		var offsetY = -1 * (posY % 33) if moveUp else posY % 33
		
		var finalPosition = Vector2(posX + offsetX, posY + offsetY)
		
		# check to make sure its still in plot bounds after moving
		if not isInPlotBounds(plotPos, finalPosition):
			queue_free()
		else:
			return finalPosition
	else:
		queue_free()
# end findPlantPosition

func grow(shotPos, distance):
	var distanceX = abs(self.global_position.x - shotPos.x)
	var distanceY = abs(self.global_position.y - shotPos.y)
	
	var isWithinDistance = true if (distanceX < distance and distanceY < distance) else false
	
	if growth < 3 and isWithinDistance:
		growth += 1
		
		if growth == 3:
			harvestCollider.disabled = false
	
		sprite.texture = load('res://assets/seeds/Turnip ' + str(growth) + '.png')
# end grow

func checkHarvestCollision(collider):
	if collider.is_in_group('player'):
		sprite.visible = false
		droppedSprite.visible = true
		pickupCollider.disabled = false
		
		isHarvestMoving = true
		harvestMoveTimer.start()
# end checkHarvestCollision

func harvestMoveOver():
	isHarvestMoving = false
# end harvetsMoveOver

func pickup(collider):
	if collider.is_in_group('player'):
		print('pickup successful')
		emit_signal('scoreUp', 100)
		queue_free()
# end onPickup