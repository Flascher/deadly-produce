extends RichTextLabel


var score = 0

func _ready():
	pass
# end _ready

func onScoreChange(scoreAdd):
	score += scoreAdd
	set_text(str(score))