extends HBoxContainer


func _ready():
	pass
# end _ready

func onSlotChange(slot):
	var slots = self.get_children()
	
	for slot in slots:
		slot.texture = load('res://assets/ui/Inventory.png')
	
	slots[slot].texture = load('res://assets/ui/Inventory_Selected.png')
# end onSlotChange