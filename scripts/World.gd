extends Node2D


enum ENEMY { SEEDGULL = 1, CROPTOP = 2, FIREVERA = 3 }

# node references
var player
var shotsGroup
var shot
var enemyGroup
var seedgull
var plantGroup
var turnip
var scoreLabel

# timers
var spawnTimer

func _ready():
	randomize()
	
	var inventoryContainer = $CanvasLayer/HUD/UIContainer/InventoryContainer
	var hpContainer = $CanvasLayer/HUD/UIContainer/VBoxContainer/HPContainer
	scoreLabel = $CanvasLayer/HUD/UIContainer/VBoxContainer/MarginContainer/HBoxContainer/ScoreLabel
	player = $Player
	shotsGroup = $Shots
	shot = preload('res://entities/shot/Shot.tscn')
	enemyGroup = $Enemies
	seedgull = preload('res://entities/seedgull/Seedgull.tscn')
	
	plantGroup = $Plants
	turnip = preload('res://entities/plants/Turnip/Turnip.tscn')
	
	# ui signals
	player.connect('slotChanged', inventoryContainer, 'onSlotChange')
	player.connect('damageTaken', hpContainer, 'onDamageTaken')
	player.connect('healed', hpContainer, 'onHealed')
	
	player.connect('shotFired', self, 'onShotFired')
	
	spawnTimer = Timer.new()
	spawnTimer.set_wait_time(5)
	spawnTimer.connect('timeout', self, 'spawnEnemy')
	
	add_child(spawnTimer)
	
	spawnTimer.start()
# end _ready

func _process(delta):
	if Input.is_action_just_pressed('restart'):
		get_tree().reload_current_scene()

func spawnEnemy():
	var visibleSpace = get_viewport().get_visible_rect()
	
	var minX = -500
	var maxX = 500
	var minY = -500
	var maxY = 500
	
	var enemyX = rand_range(minX, maxX)
	var enemyY = rand_range(minY, maxY)
	var enemyPos = Vector2(enemyX, enemyY)
	
	var newEnemy = seedgull.instance()
#	var enemyType = floor(rand_range(1, 4))
#
#	match enemyType:
#		ENEMY.SEEDGULL:
#			seedgull.instance()
	
	newEnemy.global_position = enemyPos
	newEnemy.visible = true
	newEnemy.connect('playerHit', player, 'onPlayerHit')
	newEnemy.connect('enemyDeath', self, 'onEnemyDeath')
	player.connect('enemyHit', newEnemy, 'onDamageTaken')
	enemyGroup.add_child(newEnemy)
# end spawnEnemies

func onEnemyDeath(lastPlotPos, deathPos):
	var newTurnip = turnip.instance()
	newTurnip.setPos(lastPlotPos, deathPos)
	plantGroup.add_child(newTurnip)
	player.connect('plantHarvested', newTurnip, 'dropPlant')
	newTurnip.connect('scoreUp', scoreLabel, 'onScoreChange')
	newTurnip.connect('scoreUp', player, 'raiseHealth')
# end dropSeed

func onShotFired(spawnPos, directionVec):
	var newShot = shot.instance()
	newShot.setDirectionVec(directionVec)
	newShot.setInitialPosition(spawnPos)
	
	for enemy in enemyGroup.get_children():
		newShot.connect('enemyBulletHit', enemy, 'onBulletHit')
	
	for plant in plantGroup.get_children():
		newShot.connect('waterThrown', plant, 'grow')
	
	shotsGroup.add_child(newShot)
# end onShotFired