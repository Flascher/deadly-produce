extends RichTextLabel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	self.visible = false
	pass
# end _ready

func onGameOver():
	self.visible = true
# end onGameOver