extends HBoxContainer


export var hpPerIcon = 3

var healthNode

func _ready():
	healthNode = $Health

func getTextureForHealthValue(healthValue):
	var texturePath
	
	match healthValue:
		3: texturePath = 'res://assets/ui/Full.png'
		2: texturePath = 'res://assets/ui/2 health contained.png'
		1: texturePath = 'res://assets/ui/1 health contained.png'
		0: texturePath = 'res://assets/ui/empty.png'
	
	return texturePath
# end getTextureForHealthValue

func getHealthValueForIcon(texturePath):
	var healthValue
	
	match texturePath:
		'res://assets/ui/Full.png':
			healthValue = 3
		
		'res://assets/ui/2 health contained.png':
			healthValue = 2
		
		'res://assets/ui/1 health contained.png':
			healthValue = 1
		
		'res://assets/ui/empty.png':
			healthValue = 0
	
	return healthValue
# end getHealthValueForIcon

func setHealth(playerHp):
	if playerHp >= 0 and playerHp < 9:
		var numHealthContainers = self.get_child_count()
		var iconToChange = self.get_child(playerHp / hpPerIcon)
		
		var iconTexture = getTextureForHealthValue(playerHp % hpPerIcon)
		iconToChange.texture = load(iconTexture)
# end setHealth

func onDamageTaken(playerHp):
	setHealth(playerHp)
# end onDamageTaken

func onHealed(playerHp):
	setHealth(playerHp)
# end onHealed

func onLifeGained():
	var hearts = self.get_children()
	var maxHp = hearts.size()
	
	for heart in hearts:
		heart.texture = load('res://assets/ui/Health.png')
	
	var newIcon = healthNode.duplicate()
	newIcon.name = 'Health' + str(maxHp + 1)
	
	self.add_child(newIcon)
# end onLifeGained