extends Node2D




func _ready():
	var plots = self.get_children()
	
	for plot in plots:
		plot.connect('body_entered', self, 'setLastPlot', [plot])
# end _ready

func setLastPlot(collider, plot):
	if collider.is_in_group('enemies'):
		collider.lastPlotPos = plot.global_position
# end setLastPlot